let Fun = function (f) {
    return {
        f: f,
        then: function (g) {
            return then(this, g);
        }
    };
};
let then = function (f, g) {
    return Fun(a => g.f(f.f(a)));
};
let incr = Fun(x => x + 1);
let double = Fun(x => x * 2);
let is_even = Fun(x => x % 2 == 0);
let negate = Fun(x => !x);
let not_even = is_even.then(negate);
let id_number = Fun(x => x);
let square = Fun((x) => x * x);
let isPositive = Fun((x) => x > 0);
let isEven = Fun((x) => x % 2 == 0);
let invert = Fun((x) => -x);
let squareRoot = Fun((x) => Math.sqrt(x));
let is_greater_than_zero = Fun(x => x > 0);
let id = () => Fun(x => x);
let ifThenElse = function (p, _then, _else) {
    return Fun((x) => {
        if (p.f(x)) {
            return _then.f(x);
        }
        else {
            return _else.f(x);
        }
    });
};
let f1 = incr.then(isPositive);
let f2 = incr.then(double.then(isPositive));
let f3 = ifThenElse(isPositive, squareRoot, invert.then(squareRoot));
let f4 = square.then(ifThenElse(is_even, invert, squareRoot));
// let map_countainer =
//     function <a, b>(f: Fun<a, b>, c: Countainer<a>)
//         : Countainer<b> {
//         return { content: f.f(c.content), counter: c.counter }
//     }
// let c:Countainer<number> = { content:3, counter:0 }
// let l:Countainer<boolean> = map_countainer(is_even, c)
// console.log(l.content)
let map_countainer = function (f) {
    return Fun(c => ({ content: f.f(c.content), counter: c.counter }));
};
let tick = Fun(x => ({ content: x.content, counter: incr.f(x.counter) }));
// let tick_countainer:Fun<Countainer<any>, Countainer<any>> =
//     map_countainer(tick)
let incr_countainer = map_countainer(incr).then(tick);
let is_countainer_even = map_countainer(is_even).then(tick);
let negate_countainer = map_countainer(negate).then(tick);
let is_countainer_uneven = map_countainer(is_even.then(negate)).then(tick);
let is_countainer_uneven_mapped = is_countainer_even.then(negate_countainer).then(tick);
let print_option = function print(x) {
    if (x.kind == "some")
        return `the value is ${x.value}`;
    else
        return "there is no value";
};
let none = function () { return { kind: "none" }; };
let some = function (x) {
    return { kind: "some", value: x };
};
// console.log(print_option(none<number>()))
// console.log(print_option(some<number>(10)))
let map_Option = function (f) {
    return Fun(x => x.kind == "none" ? none() : some(f.f(x.value)));
};
let incr_option = map_Option(incr);
let pipeline = map_Option(incr.then(double.then(is_greater_than_zero)));
let map_Id = function (f) {
    return f;
};
let map_Countainer_Maybe = function (f) {
    return map_countainer(map_Option(f));
};
let empty_list = function () { return { kind: "Empty" }; };
let cons_list = function (x, y) {
    return { kind: "Cons", head: x, tail: y };
};
let map_list = function (f) {
    return Fun(c => c.kind == "Empty" ? c : cons_list(f.f(c.head), map_list(f).f(c.tail)));
};
let join_list = () => null;
let transform_ascii = Fun(s => String.fromCharCode(s.x.charCodeAt(0) + s.y));
let encode_1 = Fun(x => x.kind == "Empty"
    ? x
    : cons_list(transform_ascii.f({ x: x.head, y: 1 }), encode_1.f(x.tail)));
let encode = Fun(n => Fun(x => x.kind == "Empty"
    ? x
    : cons_list(transform_ascii.f({ x: x.head, y: n }), encode.f(n).f(x.tail))));
let print_list = function (a, s) {
    if (a.kind == "Empty") {
        if (s.kind == "some") {
            console.log(s.value);
        }
        else {
            console.log("List is empty.");
        }
    }
    else {
        if (s.kind == "some") {
            print_list(a.tail, some(s.value + a.head));
        }
        else {
            print_list(a.tail, some(a.head));
        }
    }
};
let test_encode_list = cons_list("h", cons_list("e", cons_list("l", cons_list("l", cons_list("o", empty_list())))));
let test_list_2 = cons_list("b", cons_list("o", cons_list("y", cons_list("y", cons_list("o", empty_list())))));
let error = { kind: "Error", message: "Program threw an exception." };
let success = { kind: "Result", result: incr };
let world = cons_list({ kind: "Army" }, cons_list({ kind: "Terrain" }, cons_list({ kind: "Town" }, empty_list())));
let create_town = Fun(s => s.kind == "Terrain" ? { kind: "Town" } : s);
let create_army = Fun(s => s.kind == "Town" ? { kind: "Army" } : s);
let destroy_town = Fun(s => s.kind == "Town" ? { kind: "Terrain" } : s);
let world_more_towns = map_list(create_town).f(world);
let world_more_armies = map_list(create_army).f(world);
let world_destroyed_towns = map_list(destroy_town).f(world);
let map_Pair_left = function (f) {
    return Fun(c => ({ a: f(c.a), b: c.b }));
};
let map_Pair_right = function (f) {
    return Fun(c => ({ a: c.a, b: f(c.b) }));
};
let map2_Pair = function (f, g) {
    return Fun(c => ({ a: f(c.a), b: g(c.b) }));
};
let zero = (_) => 0;
//let plus = Fun<Pair<number, number>, number>(p => p.a + p.b) 
let plus = Fun(x => Fun(y => y + x));
//console.log(plus.f(10).f(21))
let zero_list = () => Fun(_ => empty_list());
let change_last = (t) => Fun(l => { if (l.kind != "Empty" && l.tail.kind == "Empty")
    return cons_list(l.head, t);
else
    return l; });
let plus_list = () => Fun(a => change_last(a.a).f(a.b));
let bbbbbb = plus_list().f({ a: test_encode_list, b: test_list_2 });
let bind = function (k) { return null; };
//# sourceMappingURL=infsen.js.map