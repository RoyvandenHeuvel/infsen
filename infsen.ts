type Fun<a, b> = { f: (i: a) => b, then: <c>(g: Fun<b, c>) => Fun<a, c> }
let Fun = function <a, b>(f: (_: a) => b): Fun<a, b> {
    return {
        f: f,
        then: function <c>(this: Fun<a, b>, g: Fun<b, c>): Fun<a, c> {
            return then(this, g)
        }
    }
}

let then = function <a, b, c>(f: Fun<a, b>, g: Fun<b, c>): Fun<a, c> {
    return Fun<a, c>(a => g.f(f.f(a)));
}

let incr = Fun<number, number>(x => x + 1)
let double = Fun<number, number>(x => x * 2)
let is_even = Fun<number, boolean>(x => x % 2 == 0)
let negate = Fun<boolean, boolean>(x => !x)
let not_even = is_even.then(negate)
let id_number = Fun<number, number>(x => x)
let square = Fun((x: number) => x * x)
let isPositive = Fun((x: number) => x > 0)
let isEven = Fun((x: number) => x % 2 == 0)
let invert = Fun((x: number) => -x)
let squareRoot = Fun((x: number) => Math.sqrt(x))
let is_greater_than_zero = Fun<number, boolean>(x => x > 0)
let id = <a>(): Fun<a, a> => Fun(x => x)

let ifThenElse =
    function <a, b>(p: Fun<a, boolean>, _then: Fun<a, b>, _else: Fun<a, b>): Fun<a, b> {
        return Fun((x: a) => {
            if (p.f(x)) {
                return _then.f(x)
            }
            else {
                return _else.f(x)
            }
        })
    }

let f1 = incr.then(isPositive)
let f2 = incr.then(double.then(isPositive))
let f3 = ifThenElse(isPositive, squareRoot, invert.then(squareRoot))
let f4 = square.then(ifThenElse(is_even, invert, squareRoot))

//type Fun<a, b> = { f: (i: a) => b, then: <c>(g: Fun<b, c>) => Fun<a, c>, repeat: <a>() => Fun<number, Fun<a, a>> }

//let repeat = function <a>(f: Fun<a, a>, n: number): Fun<a, a> {
//    if (n <= 0) {
//        return f
//    }
//    else {
//        return repeat(f, n - 1)
//    }
//}

//let Fun = function <a, b>(f: (_: a) => b): Fun<a, b> {
//    return {
//        f: f,
//        then: function <c>(this: Fun<a, b>, g: Fun<b, c>): Fun<a, c> {
//            return Fun<a, c>(a => g.f(this.f(a)))
//        },
//        repeat: function (this: Fun<a, a>): Fun<number, Fun<a, a>> {
//            return Fun<number, Fun<a, a>>(a => this)
//        }
//    }
//}

type Countainer<a> = { content: a, counter: number }

// let map_countainer =
//     function <a, b>(f: Fun<a, b>, c: Countainer<a>)
//         : Countainer<b> {
//         return { content: f.f(c.content), counter: c.counter }
//     }

// let c:Countainer<number> = { content:3, counter:0 }
// let l:Countainer<boolean> = map_countainer(is_even, c)

// console.log(l.content)

let map_countainer =
    function <a, b>(f: Fun<a, b>): Fun<Countainer<a>, Countainer<b>> {
        return Fun(c => <Countainer<b>>{ content: f.f(c.content), counter: c.counter })
    }

let tick = Fun<Countainer<any>, Countainer<any>>(x => <Countainer<any>>{ content: x.content, counter: incr.f(x.counter) })

// let tick_countainer:Fun<Countainer<any>, Countainer<any>> =
//     map_countainer(tick)

let incr_countainer: Fun<Countainer<number>, Countainer<number>> =
    map_countainer(incr).then(tick)

let is_countainer_even: Fun<Countainer<number>, Countainer<boolean>> =
    map_countainer(is_even).then(tick)

let negate_countainer: Fun<Countainer<boolean>, Countainer<boolean>> =
    map_countainer(negate).then(tick)

let is_countainer_uneven: Fun<Countainer<number>, Countainer<boolean>> =
    map_countainer(is_even.then(negate)).then(tick)

let is_countainer_uneven_mapped: Fun<Countainer<number>, Countainer<boolean>> =
    is_countainer_even.then(negate_countainer).then(tick)

type Option<a> = { kind: "none" } | { kind: "some", value: a }
let print_option = function print(x: Option<number>): string {
    if (x.kind == "some")
        return `the value is ${x.value}`
    else
        return "there is no value"
}

let none = function <a>(): Option<a> { return { kind: "none" } }
let some = function <a>(x: a): Option<a> {
    return { kind: "some", value: x }
}

// console.log(print_option(none<number>()))
// console.log(print_option(some<number>(10)))

let map_Option = function <a, b>(f: Fun<a, b>): Fun<Option<a>, Option<b>> {
    return Fun(x => x.kind == "none" ? none<b>() : some<b>(f.f(x.value)))
}

let incr_option = map_Option(incr)
let pipeline = map_Option(incr.then(double.then(is_greater_than_zero)))

type Id<a> = a
let map_Id = function <a, b>(f: Fun<a, b>): Fun<Id<a>, Id<b>> {
    return f
}

type CountainerMaybe<a> = Countainer<Option<a>>
let map_Countainer_Maybe = function <a, b>(f: Fun<a, b>):
    Fun<CountainerMaybe<a>, CountainerMaybe<b>> {
    return map_countainer<Option<a>, Option<b>>(map_Option<a, b>(f));
}

type List<a> = {
    kind: "Cons"
    head: a
    tail: List<a>
} | {
        kind: "Empty"
    }

let empty_list = function <a>(): List<a> { return { kind: "Empty" } }
let cons_list = function <a>(x: a, y: List<a>): List<a> {
    return { kind: "Cons", head: x, tail: y }
}

let map_list = function <a, b>(f: Fun<a, b>): Fun<List<a>, List<b>> {
    return Fun(c => c.kind == "Empty" ? c : cons_list(f.f(c.head), map_list(f).f(c.tail)))
}

let join_list = <a>(): Fun<List<List<a>>, List<a>> => Fun<List<List<a>>, List<a>>(a => a.kind == "Empty" ? empty_list() : a.head)

type Tuple<a, b> = { x: a, y: b }

let transform_ascii = Fun<Tuple<string, number>, string>(s => String.fromCharCode(s.x.charCodeAt(0) + s.y))

let encode_1: Fun<List<string>, List<string>> =
    Fun<List<string>, List<string>>(
        x => x.kind == "Empty"
            ? x
            : cons_list(transform_ascii.f(<Tuple<string, number>>{ x: x.head, y: 1 }), encode_1.f(x.tail))
    )

let encode: Fun<number, Fun<List<string>, List<string>>> =
    Fun<number, Fun<List<string>, List<string>>>(n => Fun<List<string>, List<string>>(x => x.kind == "Empty"
        ? x
        : cons_list(transform_ascii.f(<Tuple<string, number>>{ x: x.head, y: n }), encode.f(n).f(x.tail))))

let print_list = function (a: List<any>, s: Option<string>) {
    if (a.kind == "Empty") {
        if (s.kind == "some") {
            console.log(s.value)
        } else {
            console.log("List is empty.")
        }
    }
    else {
        if (s.kind == "some") {
            print_list(a.tail, some<string>(s.value + a.head))
        } else {
            print_list(a.tail, some<string>(a.head))
        }
    }
}

let test_encode_list = cons_list("h", cons_list("e", cons_list("l", cons_list("l", cons_list("o", empty_list())))))
let test_list_2 = cons_list("b", cons_list("o", cons_list("y", cons_list("y", cons_list("o", empty_list())))))

// print_list(test_encode_list, none<string>())
// print_list(encode_1.f(test_encode_list), none<string>())
// print_list(encode.f(4).f(test_encode_list), none<string>())
// print_list(encode.f(8).f(test_encode_list), none<string>())

type Exception<a> = { kind: "Error", message: string } | { kind: "Result", result: a }

let error = <Exception<Fun<number, number>>>{ kind: "Error", message: "Program threw an exception." }
let success = <Exception<Fun<number, number>>>{ kind: "Result", result: incr }

type Tile<a> = { kind: "Army" } | { kind: "Town" } | { kind: "Terrain" }

let world = cons_list(<Tile<any>>{ kind: "Army" }, cons_list(<Tile<any>>{ kind: "Terrain" }, cons_list(<Tile<any>>{ kind: "Town" }, empty_list())))

let create_town = Fun<Tile<any>, Tile<any>>(s => s.kind == "Terrain" ? { kind: "Town" } : s)
let create_army = Fun<Tile<any>, Tile<any>>(s => s.kind == "Town" ? { kind: "Army" } : s)
let destroy_town = Fun<Tile<any>, Tile<any>>(s => s.kind == "Town" ? { kind: "Terrain" } : s)

let world_more_towns = map_list(create_town).f(world)
let world_more_armies = map_list(create_army).f(world)
let world_destroyed_towns = map_list(destroy_town).f(world)

type Pair<a, b> = { a: a, b: b }

let map_Pair_left = function <a, b, c>(f: (_: a) => c): Fun<Pair<a, b>, Pair<c, b>> {
    return Fun(c => <Pair<c, b>>{ a: f(c.a), b: c.b })
}

let map_Pair_right = function <a, b, c>(f: (_: b) => c): Fun<Pair<a, b>, Pair<a, c>> {
    return Fun(c => <Pair<a, c>>{ a: c.a, b: f(c.b) })
}

let map2_Pair = function <a, b, c, d>(f: (_: a) => c, g: (_: b) => d): Fun<Pair<a, b>, Pair<c, d>> {
    return Fun(c => <Pair<c, d>>{ a: f(c.a), b: g(c.b) })
}

type Unit = {}

let zero = (_: Unit): number => 0
//let plus = Fun<Pair<number, number>, number>(p => p.a + p.b) 
let plus = Fun<number, Fun<number, number>>(x => Fun<number, number>(y => y + x))
//console.log(plus.f(10).f(21))

let zero_list = <a>() => Fun<Unit, List<a>>(_ => empty_list())
let change_last = <a>(t: List<a>) => Fun<List<a>, List<a>>(l => { if (l.kind != "Empty" && l.tail.kind == "Empty") return cons_list(l.head, t); else return l; })
let plus_list = <a>() => Fun<Pair<List<a>, List<a>>, List<a>>(a => change_last(a.a).f(a.b))

let bbbbbb = plus_list().f({ a: test_encode_list, b: test_list_2 })

let bind = function <a, b>(k: Fun<a, List<b>>): Fun<List<a>, List<b>> { return null }

let join_option = <a>() => Fun<Option<Option<a>>, Option<a>>(a => a.kind == "none" ? none() : a.value)

let bind_option = function <a, b>(k: Fun<a, Option<b>>): Fun<Option<a>, Option<b>> { return map_Option(k).then(join_option()) }